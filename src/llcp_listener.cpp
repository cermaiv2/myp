#include "ros/ros.h"
#include "std_msgs/String.h"
#include "mrs_msgs/Llcp.h"
#include "mrs_msgs/Float64Stamped.h"
#include <vector>
#include <stdint.h>
volatile  uint32_t out_msg_num = 0;


class SubscribeAndPublish
{
  private:
    ros::NodeHandle n_; 
  public:
  ros::Publisher pub_;
  ros::Subscriber sub_; 
  SubscribeAndPublish() {
  pub_ = n_.advertise<mrs_msgs::Float64Stamped>("maxBotix_height", 1000);
  sub_ = n_.subscribe("/uav1/llcp/received_message", 1000, &SubscribeAndPublish::chatterCallback, this);
  }

  void chatterCallback(const mrs_msgs::Llcp::ConstPtr& msg)
  {
  std::vector<unsigned char> raw = msg->payload;
  if (raw[0] == 51){
    ROS_INFO("Heartbeat");
  }
  if (raw[0] == 52)
  {
    long maxBotix_dist = raw[2]+256*raw[3];
    long grove_dist = raw[6]+256*raw[7];
    std::string str_to_print;
    for(int i=0; i < raw.size(); i++)
    {
      str_to_print += " " + std::to_string(raw[i]);   
    }
    ROS_INFO("SomeBody once told me: [%s]", str_to_print.c_str());  
    ROS_INFO("maxBotix: [%ld]", maxBotix_dist);
    ROS_INFO("Grove: [%ld]", grove_dist); 
  /* ROS_INFO_STREAM("pay "<< raw); */
  mrs_msgs::Float64Stamped out;
  out.header.seq = out_msg_num++;
  out.header.frame_id = "cermaiv2_bp";
  out.header.stamp = ros::Time::now();
  out.value = maxBotix_dist/1.0f;
  //std::stringstream ss;
  //ss << "gib headpats Publisher" << count;
  //myMsg.data = ss.str();
  pub_.publish(out);
  }
  } 
};//End of class SubscribeAndPublish
            
            
int main(int argc, char **argv)
{
    ros::init(argc, argv, "llcp_listener");
    ros::NodeHandle n; SubscribeAndPublish my_filter;
    ros::spin();
    return 0;
}



