#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>

int main(int argc, char **argv){
  ros::init(argc,argv,"myTalker");
  std_msgs::String myMsg;
  ros::NodeHandle n;
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("cum", 1000);
  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok())
    {
    std_msgs::String msg;
    std::stringstream ss;
    ss << "gib headpats Publisher" << count;
    myMsg.data = ss.str();
    chatter_pub.publish(myMsg);
    ROS_INFO("%s", myMsg.data.c_str());
    ros::spinOnce();
    loop_rate.sleep();
    count++;
    }
  return 0;
}
