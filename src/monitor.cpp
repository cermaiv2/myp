#include "ros/ros.h"
#include "std_msgs/String.h"


void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("SomeBody once told me: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Listener");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("cum", 1000, chatterCallback);
    ros::spin();
    return 0;
}

